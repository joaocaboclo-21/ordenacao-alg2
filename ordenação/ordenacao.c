#include "ordenacao.h"
#include <string.h>


void getNome(char nome[]){
	//substor[inicitua por seu nome
	strncpy(nome, "Gabriel de Paula Brasil e João Marcelo Sales Caboclo Ribeiro", MAX_CHAR_NOME);
	nome[MAX_CHAR_NOME-1] = '\0';//adicionada terminação manual para caso de overflow
}

//a função a seguir deve retornar o seu número de GRR
unsigned int getGRR(){
	return 1;
}

int buscaSequencial(int vetor[], int tam, int valor, int* numComparacoes){
	if (tam < 0)
		return -1;
	
	(*numComparacoes)++;
	if (valor == vetor[tam])
		return tam;

	return buscaSequencial(vetor, tam - 1, valor, numComparacoes);
}

// busca sequencial para usar na funcao insertion sort
int buscaSequencialInsertion(int vetor[], int tam, int valor, int* numComparacoes){
	if (tam < 0)
		return -1;
	
	(*numComparacoes)++;
	if (valor >= vetor[tam])
		return tam;
		
	return buscaSequencialInsertion(vetor, tam - 1, valor, numComparacoes);
}


//Funcao busca binaria com entradas de inicio e fim
int buscaBinariaRecursiva(int vetor[], int inicio, int fim, int valor, int* numComparacoes){
	int m = 0;

	if (inicio > fim)
		return -1;

	m = (inicio + fim)/2;
	
	(*numComparacoes)++;
	if (valor == vetor[m])
		return m;
	
	else{
		(*numComparacoes)++;
		if (valor < vetor[m])
			return buscaBinariaRecursiva(vetor, inicio, m - 1, valor, numComparacoes);
		else
			return buscaBinariaRecursiva(vetor, m + 1, fim, valor, numComparacoes);
	}

}

int buscaBinaria(int vetor[], int tam, int valor, int* numComparacoes){
	return buscaBinariaRecursiva(vetor, 0, tam, valor, numComparacoes);
}

//Funcao para trocar o elemento do inicio com o do final
void troca(int vetor[], int inicio, int fim){
	int aux;

	aux = vetor[inicio];
	vetor[inicio] = vetor[fim];
	vetor[fim] = aux;

}

//Funcao para inserir elemento no  vetor
int insere(int vetor[], int inicio, int fim){
	int numComparacoes = 0;
	int p = buscaSequencialInsertion(vetor, fim - 1, vetor[fim], &numComparacoes);
	int i = fim;

	while (i > p + 1){
		troca(vetor, i, i - 1);
		i = i - 1;
	}

	return numComparacoes;
}


int insertionSort(int vetor[], int tam){	
	int c = 0, k;
	
	if (tam < 1)
		return 0;
	
	k = insertionSort(vetor, tam - 1);
	c = insere(vetor, 0, tam);

	return c + k;
}

//Funcao para achar o menor valor do vetor, retorna o indice desse elemento
int minimo(int vetor[], int inicio, int fim, int* numComparacoes){
	int m, i;

	m = i = inicio;

	while (i < fim){
		i = i + 1;
		(*numComparacoes)++;
		if (vetor[i] < vetor[m])
			m = i;
	}
	return m;
}

//selection sort com valor de inico e fim
int selectionSortRecursivo(int vetor[], int inicio, int fim, int* numComparacoes){
	if (inicio >= fim)
		return fim;

	troca(vetor, inicio, minimo(vetor, inicio, fim, numComparacoes));
	return selectionSortRecursivo(vetor, inicio + 1, fim, numComparacoes);
}

int selectionSort(int vetor[], int tam){
	int numComparacoes = 0;
	selectionSortRecursivo(vetor, 0, tam, &numComparacoes);
	return numComparacoes;
}

//Funcao para intercalar os elementos no merge sort
int intercala(int vetor[], int inicio, int meio, int fim){
	int i, mi, k, in, temp[100000], contador = 0;

	in = inicio;
	i = inicio;
	mi = meio + 1;

	while ((in <= meio) && (mi <= fim)){
		contador++;
		if (vetor[in] <= vetor[mi]){
			temp[i] = vetor[in];
			in++;
		}
		else{
			temp[i] = vetor[mi];
			mi++;
		}
		i++;
	}
	if (in > meio){
		for (k = mi; k <= fim; k++){
			temp[i] = vetor[k];
			i++;
		}
	}
	else{
		for (k = in; k <= meio; k++){
			temp[i] = vetor[k];
			i++;
		}
	}
	for (k = inicio; k <= fim; k++){
		vetor[k] = temp[k];
	}
	
	return contador;
}

int mergeSortRecursivo(int vetor[], int inicio, int fim){
	int m = 0, c, j, k;

	if (inicio >= fim)
		return 0;
	m = (inicio + fim)/2;

	k = mergeSortRecursivo(vetor, inicio, m);
	j = mergeSortRecursivo(vetor, m + 1, fim);

	c = intercala(vetor, inicio, m, fim);
	
	return c + k + j;
}


int mergeSort(int vetor[], int tam){
	return mergeSortRecursivo(vetor, 0, tam);
}

//Funcao particiona para ser usada no quick Sort
int particiona(int vetor[], int inicio, int fim, int valor, int* numComparacoes){
	int m = inicio - 1;

	for (int i = inicio; i <= fim; i++){
		(*numComparacoes)++;
		if (vetor[i] <= valor){
			m = m + 1;
			troca(vetor, m, i);
		}
	}
	return m;
}

//Funcao quick Sort com entradas de inicio e fim
void quickSortRecursivo(int vetor[], int inicio, int fim, int* numComparacoes){
	if (inicio >= fim)
		return;
	
	int m = particiona(vetor, inicio, fim, vetor[fim], numComparacoes);
	quickSortRecursivo(vetor, inicio, m - 1, numComparacoes);
	quickSortRecursivo(vetor, m + 1, fim, numComparacoes);
}

int quickSort(int vetor[], int tam){
	int numComparacoes = 0;
	quickSortRecursivo(vetor, 0, tam, &numComparacoes);
	return numComparacoes;
}

void heapify(int vetor[], int tam, int i, int* numComparacoes){
	int par, direita, esquerda;

	par = i;
	esquerda = 2*i + 1;
	direita = 2*i + 2;

	(*numComparacoes) += 2;
	if (esquerda < tam && vetor[esquerda] > vetor[par])
		par = esquerda;

	if (direita < tam && vetor[direita] > vetor[par])
		par = direita;
	
	if (par != i){
		troca(vetor, i, par);
		heapify(vetor, tam, par, numComparacoes);
	}

}

int heapSort(int vetor[], int tam){
	int i, numComparacoes = 0;

	for (i = tam / 2 - 1; i >= 0; i--)
		heapify(vetor, tam, i, &numComparacoes);
	
	for (i = tam; i >= 0; i--){
		troca(vetor, 0, i);
		heapify(vetor, i, 0, &numComparacoes);
	}
	return numComparacoes;
}
