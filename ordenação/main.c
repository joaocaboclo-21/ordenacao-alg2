#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ordenacao.h"

void menu(){
	printf("\nDigite: \n[1] para testar Busca Sequencial\n[2] para testar Busca Binaria\n[3] para ordenar vetor com Insertion Sort\n[4] para ordenar vetor com Selection Sort\n[5] para ordenar vetor com Merge Sort\n[6] para ordenar vetor com Quick Sort\n[7] para ordenar vetor com Heap Sort\n[0] para terminar o programa\nQual sua escolha? ");
}

void printa_vetor(int vetor[], int tam){
	int i;
	printf("\n");

	for (i = 0; i < tam; i++)
		printf("%d ", vetor[i]);
	
	printf("\n");
}

int *cria_vetor(int quantidade){
	int i, opcao;
	int* vetor = malloc(quantidade * sizeof(int));
	if(vetor == NULL){
		printf("Falha fatal. Impossível alocar memoria.");
		return NULL;
	}

	printf("\n");
	printf("Digite [1] para obter um vetor de números aleatórios ou [2] para digitar os números: ");
	scanf("%d", &opcao);
	printf("\n");

	if (opcao == 1){
		for (i = 0; i < quantidade; i++){
			vetor[i] = rand() % 100;
		}
		printa_vetor(vetor, quantidade);
		printf("\n");
	}
	else if(opcao == 2){
		int valor;
		for(i = 0; i < quantidade; i++){
			printf("Digite o número da posição [%d]: ", i);
			scanf("%d", &valor);
			vetor[i] = valor;
		}
	}
	return vetor;

}

int *liberaVetor(int *vetor){
	free(vetor);
	vetor = NULL;
	return vetor;
}

int main(){
	srand(0);
	int resposta, *vetor, busca, quantidade, numComparacoes, posicao, c;

	clock_t start, end;
	double total;

	menu();
	scanf("%d", &resposta);

	while(resposta != 0)
	{
		printf("\n");
		printf("Quantos numeros tera o vetor? ");
		scanf("%d", &quantidade);
		printf("\n");
	    numComparacoes = 0;

		if(resposta == 1){
			vetor = cria_vetor(quantidade);
			printf("Qual o numero buscado? ");
			scanf("%d", &busca);
			
			start = clock();
			posicao = buscaSequencial(vetor, quantidade - 1, busca, &numComparacoes);
			end = clock();

			total = ((double)end - start)/CLOCKS_PER_SEC;

			if(posicao == -1){
				printf("O numero nao foi encontrado no vetor\n");
			}
			else{
				printf("O indice do numero é %d \n", posicao);
			}
			printf("Foram feitas um total de %d comparações\n", numComparacoes);
			printf("\nTempo total: %f \n", total);
		}


		else if(resposta == 2){
			vetor = cria_vetor(quantidade);

			printf("Ordenando o vetor...");
			c = quickSort(vetor, quantidade - 1);

			printa_vetor(vetor, quantidade);
			
			printf("Qual o numero buscado? ");
			scanf("%d", &busca);

			start = clock();
			posicao = buscaBinaria(vetor, quantidade - 1, busca, &numComparacoes);
			end = clock();

			total = ((double)end - start)/CLOCKS_PER_SEC;

			if(posicao == -1){
				printf("O numero nao foi encontrado no vetor\n");
			}
			else{
				printf("O indice do numero é %d \n", posicao);
			}
			printf("Foram feitas um total de %d comparações\n", numComparacoes);
			printf("\nTempo total: %f \n", total);
		}


		else if(resposta == 3){
			vetor = cria_vetor(quantidade);

			start = clock();
			c = insertionSort(vetor, quantidade - 1);
			end = clock();

			total = ((double)end - start)/CLOCKS_PER_SEC;

			printf("Ordenando...");
			printa_vetor(vetor, quantidade);

			printf("Foram feitas %d comparações\n", c);
			printf("\nTempo total: %f \n", total);
		}


		else if(resposta == 4){
			vetor = cria_vetor(quantidade);
			
			start = clock();
			c = selectionSort(vetor, quantidade - 1);
			end = clock();

			total = ((double)end - start)/CLOCKS_PER_SEC;

			printf("Ordenando...");
			printa_vetor(vetor, quantidade);

			printf("Foram feitas %d comparações\n", c);
			printf("\nTempo total: %f \n", total);
		}


		else if(resposta == 5){
			vetor = cria_vetor(quantidade);
			
			start = clock();
			c = mergeSort(vetor, quantidade - 1);
			end = clock();

			total = ((double)end - start)/CLOCKS_PER_SEC;

			printf("Ordenando...");
			printa_vetor(vetor, quantidade);

			printf("Foram feitas %d comparações\n", c);
			printf("\nTempo total: %f \n", total);
		
		}


		else if(resposta == 6){
			vetor = cria_vetor(quantidade);
			
			start = clock();
			c = quickSort(vetor, quantidade - 1);
			end = clock();

			total = ((double)end - start)/CLOCKS_PER_SEC;

			printf("Ordenando...");
			printa_vetor(vetor, quantidade);

			printf("Foram feitas %d comparações\n", c);
			printf("\nTempo total: %f \n", total);




		}	
		else if(resposta == 7){
			vetor = cria_vetor(quantidade);
			
			start = clock();
			c = heapSort(vetor, quantidade - 1);
			end = clock();

			total = ((double)end - start)/CLOCKS_PER_SEC;

			printf("Ordenando...");
			printa_vetor(vetor, quantidade);

			printf("Foram feitas %d comparações\n", c);
			printf("\nTempo total: %f \n", total);

		}


		else
			printf("Número inválido! Digite outro número");
		

		c = 0;
		total = 0;
		vetor = liberaVetor(vetor);
		menu();
		scanf("%d", &resposta);
	}
	printf("Finalizando...");
	return 1;
} 
